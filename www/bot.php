<?php

require("init.php");

if ($_GET['rel'])
{
  echo "\n";
  echo "-------------------------\n";
  echo " Newest Releases:\n";
  echo "-------------------------\n";

  files_listnewreleases($data,20);
  foreach($data as $id)
  {
    echo "  ".files_getlastchange($id)." - ".files_getname($id)." by ".files_getassigned($id)."\n";
  }
  
  echo "-------------------------\n";
  echo " http://orangepluto.sus.sun.ac.za/release/\n";
  echo "-------------------------";
}
else if ($_GET['announce'])
{
  $time=$_GET['time'];
  $lastid=$_GET['lastid'];
  date_default_timezone_set("Africa/Johannesburg");
  
  if ($time=="")
  {
    $time=date("Y-m-d_H:i:s");
    $lastid=0;
  }
  
  $id=files_getrelease($time,$lastid);
  if ($id==0)
    echo date("Y-m-d_H:i:s")."|".$lastid."|-";
  else
    echo $time."|".$id."|New Release: ".files_getname($id)." by ".files_getassigned($id);
}
else if ($_GET['search'])
{
  if ($_GET['q']!="")
  {
    $filter=$_GET['q'];
    echo "\n";
    echo "-------------------------\n";
    echo " Search Results for '".$filter."':\n";
    echo "-------------------------\n";
    files_search($results,$filter);
    $count=0;
    foreach($results as $id)
    {
      if ($count>=10)
      {
        $count=-1;
        break;
      }
      else
      {
        $status=files_getstatus($id);
        if ($status=="WAITING")
          $prettystatus="[REQUEST]";
        elseif ($status=="BUSY")
          $prettystatus="[CLAIMED]";
        elseif ($status=="DONE")
          $prettystatus="[RELEASE]";
        $item=$prettystatus." ".files_getname($id);
        if ($status!="WAITING")
          $item.=" by ".files_getassigned($id);
        echo "  ".$item."\n";
        $count+=1;
      }
    }
    if ($count==0)
      echo "   (no results)\n";
    if ($count==-1)
      echo "   (results limited)\n";
    echo "-------------------------\n";
    echo " http://orangepluto.sus.sun.ac.za/release/search.php?f=".urlencode($filter)."\n";
    echo "-------------------------";
  }
  else
  {
    echo "please provide a search string, eg: 'big bang #series !release'";
  }
}
else
{
  echo "bot interface!<br/>";
  echo "use the normal site <a href='index.php'>here</a>";
}


