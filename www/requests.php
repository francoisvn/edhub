<?php

require("init.php");
require("header.php");

echo "<h1>edhub requests</h1><br/>";
require("menu.php");

if ($_GET['act']=="new")
{
  if ($_GET['name']!="")
  {
    $id=files_getid($_GET['name']);
    if ($id!=0)
    {
      echo "request for '".html_safe($_GET['name'])."' has already been made. see <a href='view.php?id=".$id."'>here</a><br/>";
    }
    else
    {
      $id=files_new($_GET['name'],"WAITING");
      echo "new request <a href='view.php?id=".$id."'>'".html_safe($_GET['name'])."'</a> added<br/>";
    }
  }
  echo "<form action='requests.php' method='get'>";
  echo "<input type='hidden' name='act' value='new'/>";
  echo "<label>new request: </label>";
  echo "<input type='text' name='name' value=''/> ";
  echo "<input type='submit' value='request'/><br/>";
  echo "</form>";
  echo "please first read the <a href='help.php'>help</a> before making a request<br/>\n";
}
else
{
  echo "<a href='requests.php?act=new'>make a request</a><br/><br/>";
  
  files_list($data,$_GET['filter'],"WAITING");
  foreach($data as $id)
  {
    echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))."</a> (<a href='busy.php?id=".$id."&act=claim'>claim</a>)<br/>";
  }
}

require("footer.php");
