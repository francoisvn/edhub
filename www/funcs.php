<?php

function db_connect()
{
  $dbcon=mysql_connect("localhost","edhub","edhub");
  if (!$dbcon){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  mysql_select_db("edhub", $dbcon);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function sql_safe($arg)
{
  return mysql_real_escape_string($arg);
}

function sql_unsafe($arg)
{
  return stripslashes($arg);
}

function html_safe($arg)
{
  return htmlentities($arg);
}

function log_write($text)
{
  if ($_SESSION['user_id']!=0)
  {
    $user="User: ".sql_safe(user_getname($_SESSION['user_id']))." ";
  }
  else
  {
    $user="";
  }
  mysql_query("INSERT INTO log (ip, hostname, msg) VALUES ('".$_SERVER['REMOTE_ADDR']."', '".sql_safe($_SERVER['REMOTE_HOST'])."', '".$user.sql_safe($text)."');");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function log_print()
{
  $result = mysql_query("SELECT * FROM log");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  echo "<div id='log'>";
  echo "<table border='1' cellpadding='5' width='100%' cellspacing='0' style='border-color: black; border-style: solid;'>";
  $grey=true;
  while($row = mysql_fetch_array($result))
  {
    echo "<tr ";
    $msg=$row['msg'];
    if (stripos(" ".$msg,"error")==1 or stripos(" ".$msg,"exception")==1)
    {
      echo "bgcolor=red";
    }
    else if ($grey)
    {
      echo "bgcolor='#BFBFBF'";
    }
    else
    {
      echo "bgcolor='#E5E5E5'";
    }
    $grey= !$grey;
    echo ">";
    echo "<td style='border: 0;'>";
    echo $row['time'];
    echo "</td>";
    echo "<td style='border: 0;'>";
    echo $row['hostname']."(".$row['ip'].")";
    echo "</td>";
    echo "<td style='border: 0;'>";
    echo $msg;
    echo "</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "</div";
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function user_loggedin()
{
  if ($_SESSION['user_id']!="")
  {
    return true;
  }
  else
  {
    return false;
  }
}

function user_login($username,$pass)
{
  $_SESSION['user_id']=0;
  $result = mysql_query("SELECT * FROM users WHERE username='".sql_safe($username)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  if (!$result)
  {
    return false;
  }
  
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  if ($row['passhash']==md5($pass))
  {
    $_SESSION['user_id']=$row['id'];
    return true;
  }
  else
  {
    return false;
  }
}

function user_logout()
{
  $_SESSION['user_id']=0;
}

function user_getname($id)
{
  $result = mysql_query("SELECT username FROM users WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['username']);
}

function user_new($username,$password)
{
  mysql_query("INSERT INTO users (username, passhash) VALUES ('".sql_safe($username)."', '".sql_safe(md5($password))."');");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function user_remove($username)
{
  mysql_query("DELETE FROM users WHERE username='".sql_safe($username)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function log_clear()
{
  mysql_query("DELETE FROM log");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function users_list(&$data)
{
  $data=array();
  $result = mysql_query("SELECT username FROM users");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['username']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function error_handler($errno, $errstr)
{
  if ($errno!=8)
  {
    log_write("Error:(".$errno.") ".$errstr);
    echo "<b>Error! </b> (".$errno.") ".$errstr."<br/>";
    echo "Ending Script";
    die();
  }
}

function exception_handler($exception)
{
  log_write("Exception: ".$exception->getMessage());
  echo "<b>Exception! </b> ".$exception->getMessage();
  echo "Ending Script";
  die();
}

function db_optimize()
{
  mysql_query("OPTIMIZE TABLE log, files, users ");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function files_getname($id)
{
  $result = mysql_query("SELECT name FROM files WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['name']);
}

function files_getid($name)
{
  $result = mysql_query("SELECT id FROM files WHERE name='".sql_safe($name)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['id']);
}

function files_getstatus($id)
{
  $result = mysql_query("SELECT status FROM files WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['status']);
}

function files_getassigned($id)
{
  $result = mysql_query("SELECT assigned FROM files WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['assigned']);
}

function files_getlastchange($id)
{
  $result = mysql_query("SELECT lastchange FROM files WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['lastchange']);
}

function files_list(&$data,$filter,$status="",$limit=0)
{
  if ($filter!="")
  {
    $filter="WHERE name LIKE '%".sql_safe($filter)."%' ";
    if ($status!="")
    {
      $filter.="AND status='".sql_safe($status)."' ";
    }
  }
  elseif ($status!="")
  {
    $filter="WHERE status='".sql_safe($status)."' ";
  }
  if ($limit!=0)
  {
    $limit=" LIMIT ".sql_safe($limit);
  }
  else
  {
    $limit="";
  }
  $data=array();
  $result = mysql_query("SELECT id FROM files ".$filter."ORDER BY lastchange DESC".$limit);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['id']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function files_listnewreleases(&$data,$limit=0)
{
  if ($limit!=0)
  {
    $limit=" LIMIT ".sql_safe($limit);
  }
  else
    $limit="";
  
  $data=array();
  $result = mysql_query("SELECT id FROM (SELECT id,lastchange FROM files WHERE status='DONE' ORDER BY lastchange DESC".$limit.") AS tbl ORDER BY tbl.lastchange ASC");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['id']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function files_new($name,$status,$assigned="")
{
  if ($assigned!="")
  {
    $assigned1=", assigned";
    $assigned2=", '".sql_safe($assigned)."'";
  }
  mysql_query("INSERT INTO files ( name, status".$assigned1.") VALUES ('".sql_safe($name)."', '".sql_safe($status)."'".$assigned2.");");
  $ret=mysql_insert_id();
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($ret);
}

function files_edit($id,$name,$status,$assigned="")
{
  if ($assigned!="")
  {
    $assigned=", assigned='".sql_safe($assigned)."'";
  }
  mysql_query("UPDATE files SET name='".sql_safe($name)."', status='".sql_safe($status)."'".$assigned.", lastchange=CURRENT_TIMESTAMP WHERE id='".sql_safe($id)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function files_remove($id)
{
  mysql_query("DELETE FROM tags WHERE fileid='".sql_safe($id)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  mysql_query("DELETE FROM files WHERE id='".sql_safe($id)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function files_getrelease($time,$lastid)
{
  $auxtime="";
  if ($lastid!=0)
    $auxtime=" AND lastchange>'".sql_safe(files_getlastchange($lastid))."' ";
  $result = mysql_query("SELECT id FROM files WHERE status='DONE' AND lastchange>='".sql_safe($time)."'".$auxtime." ORDER BY lastchange ASC LIMIT 1");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['id']);
}

function tags_preptag($tag)
{
  return ereg_replace("[^a-z0-9]","",strtolower($tag));
}

function tags_new($fileid,$tag)
{
  $tag=tags_preptag($tag);
  if (tags_filetagged($fileid,$tag))
    return -1;
  mysql_query("INSERT INTO tags ( fileid, tag ) VALUES ('".sql_safe($fileid)."', '".sql_safe($tag)."');");
  $ret=mysql_insert_id();
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($ret);
}

function tags_remove($id)
{
  mysql_query("DELETE FROM tags WHERE id='".sql_safe($id)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function tags_getfileid($id)
{
  $result = mysql_query("SELECT fileid FROM tags WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['fileid']);
}

function tags_gettag($id)
{
  $result = mysql_query("SELECT tag FROM tags WHERE id='".sql_safe($id)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['tag']);
}

function tags_listperfileid(&$data,$fileid)
{
  $data=array();
  $result = mysql_query("SELECT id FROM tags WHERE fileid='".sql_safe($fileid)."' ORDER BY tag ASC ");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['id']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function tags_listpertag(&$data,$tag)
{
  $tag=tags_preptag($tag);
  $data=array();
  $result = mysql_query("SELECT tags.id FROM tags, files WHERE tags.tag='".sql_safe($tag)."' AND files.id=tags.fileid ORDER BY files.name ASC ");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['id']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function tags_listcloud(&$data,$limit="",$fileidtoignore=0)
{
  if ($limit!="")
    $limit=" LIMIT ".sql_safe($limit);
  if ($fileidtoignore!=0)
    $filefilter=" WHERE tag NOT IN (SELECT tag FROM tags WHERE fileid=".sql_safe($fileidtoignore).")";
  else
    $filefilter="";
  
  $data=array();
  $result = mysql_query("SELECT tag FROM tags".$filefilter." GROUP BY tag ORDER BY COUNT(id) DESC".$limit);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  
  while($row = mysql_fetch_array($result))
  {
    array_push($data,sql_unsafe($row['tag']));
  }
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
}

function tags_tagcount($tag)
{
  $tag=tags_preptag($tag);
  $result = mysql_query("SELECT COUNT(id) FROM tags WHERE tag='".sql_safe($tag)."'");
  $row = mysql_fetch_array($result);
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  return sql_unsafe($row['COUNT(id)']);
}

function tags_filetagged($fileid,$tag)
{
  $tag=tags_preptag($tag);
  $result = mysql_query("SELECT COUNT(id) FROM tags WHERE fileid='".sql_safe($fileid)."' AND tag='".sql_safe($tag)."'");
  if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  $row = mysql_fetch_array($result);
  $count = $row['COUNT(id)'];
  
  if ($count>0)
    return true;
  else
    return false;
}

function search_score($searchstring,$id)
{
  $score=0;
  $searchstring=trim($searchstring);
  $searchwords=explode(" ",$searchstring);
  
  $filetext=" ".files_getname($id)." ";
  $filestatus=files_getstatus($id);
  $tagstext=" ";
  tags_listperfileid($data,$id);
  foreach($data as $tagid)
  {
    $tagstext.=tags_gettag($tagid)." ";
  }
  
  $filetext=strtolower($filetext);
  $tagstext=strtolower($tagstext);
  $statuswords=array();
  
  foreach($searchwords as $word)
  {
    $word=strtolower($word);
    $basescore=1.0;
    if (strlen($word)<4)
      $basescore=0.5;
    
    if (substr($word,0,1)=='#')
    {
      $word=substr($word,1);
      if (strpos($tagstext," ".$word." ")!==false)
        $score+=$basescore+0.2;
      else if (strpos($tagstext,$word)!==false)
        $score+=$basescore;
    }
    else if (substr($word,0,1)=='!')
    {
      $word=substr($word,1);
      if ($word=="request")
        array_push($statuswords,"WAITING");
      elseif ($word=="claimed")
        array_push($statuswords,"BUSY");
      elseif ($word=="release")
        array_push($statuswords,"DONE");
    }
    else
    {
      if (strpos($filetext," ".$word." ")!==false)
        $score+=$basescore+0.2;
      else if (strpos($filetext,$word)!==false)
        $score+=$basescore;
    }
  }
  
  foreach($statuswords as $word)
  {
    if ($word==$filestatus)
      $score*=1.5;
  }
  
  //return $score*100000+$id; //make unique for each id
  return $score*100;
}

function files_search(&$searchresults,$searchstring)
{
  $searchresults=array();
  $searchstring=trim($searchstring);
  if ($searchstring=="")
    return;
  
  $searchwords=explode(" ",$searchstring);
  $wherefiles="";
  $wheretags="";
  
  foreach($searchwords as $word)
  {
    $word=strtolower($word);
    
    if (substr($word,0,1)=='#')
    {
      $word=substr($word,1);
      if ($wheretags!="")
        $wheretags.=" OR ";
      $wheretags.="LOWER(tag) LIKE '%".sql_safe($word)."%'";
    }
    else if (substr($word,0,1)=='!')
    {
      //ignore special tags now
    }
    else
    {
      if ($wherefiles!="")
        $wherefiles.=" OR ";
      $wherefiles.="LOWER(name) LIKE '%".sql_safe($word)."%'";
    }
  }
  
  $prelimresults=array();
  
  if ($wherefiles!="")
  {
    $result = mysql_query("SELECT id FROM files WHERE ".$wherefiles);
    if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
    
    while($row = mysql_fetch_array($result))
    {
      array_push($prelimresults,sql_unsafe($row['id']));
    }
    if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  }
  
  if ($wheretags!="")
  {
    $result = mysql_query("SELECT fileid FROM tags WHERE ".$wheretags);
    if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
    
    while($row = mysql_fetch_array($result))
    {
      $fileid=sql_unsafe($row['fileid']);
      if (!in_array($fileid,$searchresults))
        array_push($prelimresults,$fileid);
    }
    if (mysql_error()!=null){trigger_error("DB Error! ".mysql_error(),E_USER_ERROR);}
  }
  
  $scoredresults=array();
  foreach($prelimresults as $id)
  {
    $scoredresults[search_score($searchstring,$id)."_".$id]=$id;
  }
  
  krsort($scoredresults,SORT_NUMERIC);
  foreach ($scoredresults as $k => $v)
  {
    array_push($searchresults,$v);
  }
}


