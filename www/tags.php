<?php

require("init.php");
require("header.php");

echo "<h1>edhub tags</h1><br/>";
require("menu.php");

if ($_GET['act']=="add")
{
  $fileid=$_GET['id'];
  $tag=$_GET['tag'];
  
  if ($tag!="")
  {
    $tags=explode(",",$tag);
    foreach($tags as $tag)
    {
      $tag=tags_preptag($tag);
      tags_new($fileid,$tag);
      echo "tag <a href='tags.php?tag=".html_safe(tags_preptag($tag))."'>'".html_safe(tags_preptag($tag))."'</a> added to <a href='view.php?id=".html_safe($fileid)."'>'".html_safe(files_getname($fileid))."'</a><br/>";
    }
    echo "<a href='tags.php?act=add&id=".html_safe($fileid)."'>add more tags</a><br/>";
  }
  else
  {
    echo "<form action='tags.php' method='get'>";
    echo "<input type='hidden' name='act' value='add'/>";
    echo "<input type='hidden' name='id' value='".html_safe($fileid)."'/>";
    echo "<label>add tag(s) to '".html_safe(files_getname($fileid))."': </label>";
    echo "<input type='text' id='tag' name='tag' value='' size='35'/> ";
    echo "<input type='submit' value='add'/><br/>";
    echo "popular tags: ";
    tags_listcloud($data,10,$fileid);
    foreach($data as $tag)
    {
      //echo "<a href='tags.php?act=add&id=".html_safe($fileid)."&tag=".html_safe($tag)."'>".html_safe($tag)."</a> ";
      echo "<a href='javascript:add_tag(\"".html_safe($tag)."\")'>".html_safe($tag)."</a> ";
    }
    echo "<br/>seperate your tags with a comma";
    echo "</form>";
  }
}
else if ($_GET['act']=="remove")
{
  $id=$_GET['id'];
  $fileid=tags_getfileid($id);
  $tag=tags_gettag($id);
  
  tags_remove($id);
  echo "tag <a href='tags.php?tag=".html_safe($tag)."'>'".html_safe($tag)."'</a> removed from <a href='view.php?id=".html_safe($fileid)."'>'".html_safe(files_getname($fileid))."'</a><br/>";
  
}
else if ($_GET['tag']!="")
{
  $tag=$_GET['tag'];
  echo "<b>files with tag '".html_safe($tag)."':</b><br/>";
  tags_listpertag($data,$tag);
  foreach($data as $id)
  {
    $fileid=tags_getfileid($id);
    echo "<a href='view.php?id=".html_safe($fileid)."'>".html_safe(files_getname($fileid))."</a> (<a href='tags.php?act=remove&id=".$id."'>remove</a>)<br/>";
  }
}
else
{
  tags_listcloud($data);
  foreach($data as $tag)
  {
    echo "<a href='tags.php?tag=".html_safe($tag)."'>".html_safe($tag)." (".tags_tagcount($tag).")</a><br/>";
  }
}

require("footer.php");
