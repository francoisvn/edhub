<?php

require("init.php");
require("header.php");

echo "<h1>edhub home</h1><br/>";
require("menu.php");

echo "<b>edhub release system</b><br/>\n";

echo "please first read the <a href='help.php'>help</a><br/>\n";

echo "<br/><b>newest 20 releases:</b><br/>";
files_list($data,"","DONE",20);
foreach($data as $id)
{
  echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))." by ".html_safe(files_getassigned($id))."</a><br/>";
}
echo "<a href='done.php'>more...</a><br/>";

echo "<br/><b>currently being downloaded:</b><br/>";
files_list($data,"","BUSY",20);
foreach($data as $id)
{
  echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))." by ".html_safe(files_getassigned($id))."</a><br/>";
}
echo "<a href='busy.php'>more...</a><br/>";

echo "<br/><b>new requests:</b><br/>";
files_list($data,"","WAITING",20);
foreach($data as $id)
{
  echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))."</a><br/>";
}
echo "<a href='requests.php'>more...</a><br/>";

require("footer.php");
