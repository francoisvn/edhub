<?php

require("init.php");
require("header.php");

echo "<h1>edhub releases</h1><br/>";
require("menu.php");

if ($_GET['act']=="new")
{
  if ($_GET['name']!="")
  {
    $id=files_getid($_GET['name']);
    if ($id!=0)
    {
      echo "'".html_safe($_GET['name'])."' has already been downloaded. see <a href='view.php?id=".$id."'>here</a><br/>";
    }
    else
    {
      $id=files_new($_GET['name'],"DONE",$_GET['assigned']);
      echo "new release <a href='view.php?id=".$id."'>'".html_safe($_GET['name'])."'</a> added<br/>";
    }
  }
  echo "<form action='done.php' method='get'>";
  echo "<input type='hidden' name='act' value='new'/>";
  echo "<label>new release: </label>";
  echo "<input type='text' name='name' value=''/> ";
  echo "<label>by: </label>";
  echo "<input type='text' name='assigned' value='' size='15'/> ";
  echo "<input type='submit' value='release'/><br/>";
  echo "</form>";
  echo "please first read the <a href='help.php'>help</a> before releasing<br/>\n";
}
else if ($_GET['act']=="done")
{
  if ($_GET['id']!="")
  {
    $id=$_GET['id'];
    files_edit($id,files_getname($id),"DONE");
    echo "marked '".html_safe(files_getname($id))."' by '".html_safe(files_getassigned($id))."' as done";
  }
  else
  {
    echo "invalid id";
  }
}
elseif ($_GET['act']=="drop")
{
  if ($_GET['id']!="")
  {
    $id=$_GET['id'];
    files_edit($id,files_getname($id),"BUSY");
    echo "marked '".html_safe(files_getname($id))."' as incomplete";
  }
  else
  {
    echo "invalid id";
  }
}
else
{
  echo "<a href='done.php?act=new'>make a release</a><br/><br/>";
  
  files_list($data,"","DONE");
  foreach($data as $id)
  {
    echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))." by ".html_safe(files_getassigned($id))."</a> (<a href='done.php?act=drop&id=".$id."'>not done</a>)<br/>";
  }
}

require("footer.php");
