<?php

require("init.php");
require("header.php");

echo "<h1>edhub claims</h1><br/>";
require("menu.php");

if ($_GET['act']=="claim")
{
  if ($_GET['id']!="")
  {
    $id=$_GET['id'];
    if ($_GET['assigned']!="")
    {
      files_edit($id,files_getname($id),"BUSY",$_GET['assigned']);
      echo "claimed request '".html_safe(files_getname($id))."' by '".html_safe($_GET['assigned'])."'";
    }
    else
    {
      echo "<form action='busy.php' method='get'>";
      echo "<input type='hidden' name='act' value='claim'/>";
      echo "<input type='hidden' name='id' value='".$id."'/>";
      echo "<label>".html_safe(files_getname($id))." will be downloaded by: </label>";
      echo "<input type='text' name='assigned' value=''/> ";
      echo "<input type='submit' value='claim'/><br/>";
      echo "</form>";
    }
  }
  else
  {
    echo "invalid id";
  }
}
elseif ($_GET['act']=="drop")
{
  if ($_GET['id']!="")
  {
    $id=$_GET['id'];
    files_edit($id,files_getname($id),"WAITING");
    echo "dropped claimed request '".html_safe(files_getname($id))."'";
  }
  else
  {
    echo "invalid id";
  }
}
else
{
  files_list($data,"","BUSY");
  foreach($data as $id)
  {
    echo "<a href='view.php?id=".$id."'>".html_safe(files_getlastchange($id))." - ".html_safe(files_getname($id))." will be downloaded by ".html_safe(files_getassigned($id))."</a> (<a href='busy.php?act=drop&id=".$id."'>drop</a>) (<a href='done.php?act=done&id=".$id."'>done</a>)<br/>";
  }
}

require("footer.php");
