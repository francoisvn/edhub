<?php

require("init.php");
require("header.php");

echo "<h1>edhub search</h1><br/>";
require("menu.php");

echo "<form action='search.php' method='get'>";
echo "<input type='text' name='f' value='".html_safe($_GET['f'])."'/>";
echo "<input type='submit' value='go'/><br/>";
echo "</form>";

if ($_GET['f']!="")
{
  $filter=$_GET['f'];
  echo "<h3>results for '".html_safe($filter)."':</h3>";
  files_search($results,$filter);
  foreach($results as $id)
  {
    $status=files_getstatus($id);
    if ($status=="WAITING")
      $prettystatus="[REQUEST]";
    elseif ($status=="BUSY")
      $prettystatus="[CLAIMED]";
    elseif ($status=="DONE")
      $prettystatus="[RELEASE]";
    echo "<a href='view.php?id=".$id."'>".$prettystatus." ".html_safe(files_getname($id))."</a><br/>";
  }
}
else
{
  echo "eg: 'big bang #series !release'";
}

require("footer.php");
