<?php

session_start();

require("funcs.php");

db_connect();
log_write("Page: ".$_SERVER['REQUEST_URI']."\n");

//set_error_handler("error_handler",E_ERROR or E_WARNING or E_PARSE or E_CORE_ERROR or E_CORE_WARNING or E_COMPILE_ERROR or E_COMPILE_WARNING or E_USER_ERROR or E_USER_WARNING or E_RECOVERABLE_ERROR);
set_error_handler("error_handler");
set_exception_handler("exception_handler");

$_SESSION['headextra']="";
$_SESSION['div_page']="";

