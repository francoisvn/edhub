<?php

require("init.php");
require("header.php");

echo "<h1>edhub viewer</h1><br/>";
require("menu.php");

if ($_GET['id']=="")
{
  echo "invalid id";
}
else
{
  $id=strip_tags($_GET['id']);
  $status=files_getstatus($id);
  if ($status=="WAITING")
  {
    echo "requested file: ".html_safe(files_getname($id));
    echo " (<a href='busy.php?id=".$id."&act=claim'>claim</a>)";
  }
  elseif ($status=="BUSY")
  {
    echo "claimed file: ".html_safe(files_getname($id))." assigned to: ".html_safe(files_getassigned($id));
    echo " (<a href='busy.php?id=".$id."&act=drop'>drop</a>) (<a href='done.php?act=done&id=".$id."'>done</a>)";
  }
  elseif ($status=="DONE")
  {
    echo "released file: ".html_safe(files_getname($id))." by: ".html_safe(files_getassigned($id));
    echo " (<a href='done.php?act=drop&id=".$id."'>not done</a>)";
  }
  echo "<br/>";
  echo "last changed: ".html_safe(files_getlastchange($id))."<br/>";
  if (user_loggedin())
  {
    echo "admin: (<a href='admin.php?act=files&act2=edit&stage=1&id=".$id."'>edit</a>) (<a href='admin.php?act=files&act2=remove&id=".$id."'>remove</a>)<br/>";
  }
  echo "tags: ";
  tags_listperfileid($data,$id);
  foreach($data as $tagid)
  {
    $tag=tags_gettag($tagid);
    echo "<a href='tags.php?tag=".html_safe($tag)."'>".html_safe($tag)."</a> (<a href='tags.php?act=remove&id=".$tagid."'>remove</a>) ";
  }
  echo "(<a href='tags.php?act=add&id=".$id."'>add tag</a>)<br/>";
}

require("footer.php");
