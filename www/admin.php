<?php

require("init.php");
if ($_GET['act']=="log")
{
  $_SESSION['div_page']="page_log";
}

if ($_GET['act']=="logout")
{
  user_logout();
}
if (!user_loggedin())
{
  user_login($_POST['username'],$_POST['password']);
}

require("header.php");

echo "<h1>edhub admin</h1><br/>";
require("menu.php");

if (user_loggedin())
{
  echo "&gt;\n";
  
  if ($_GET['act']=="users")
  {
    echo "<a href='admin.php?act=users&act2=list'>list users</a> | ";
    echo "<a href='admin.php?act=users&act2=new'>new user</a> | ";
    echo "<a href='admin.php?act=users&act2=remove'>remove user</a><br/>\n";
    
    if ($_GET['act2']=="new")
    {
      if ($_GET['username']!="")
      {
        user_new($_GET['username'],$_GET['password']);
        echo "<br/>";
        echo "new user '".html_safe($_GET['username'])."' with password '".html_safe($_GET['password'])."' added";
      }
      echo "<form action='admin.php' method='get'>";
      echo "<input type='hidden' name='act' value='users'/><br/>";
      echo "<input type='hidden' name='act2' value='new'/><br/>";
      echo "<label>new username: </label>";
      echo "<input type='text' name='username' value=''/><br/>";
      echo "<label>new password: </label>";
      echo "<input type='password' name='password'/><br/>";
      echo "<input type='submit' value='add user'/><br/>";
      echo "</form>";
    }
    else if ($_GET['act2']=="remove")
    {
      if ($_GET['username']!="")
      {
        user_remove($_GET['username']);
        echo "<br/>";
        echo "removed user '".html_safe($_GET['username'])."'";
      }
      echo "<form action='admin.php' method='get'>";
      echo "<input type='hidden' name='act' value='users'/><br/>";
      echo "<input type='hidden' name='act2' value='remove'/><br/>";
      echo "<label>username: </label>";
      echo "<input type='text' name='username' value=''/><br/>";
      echo "<input type='submit' value='remove user'/><br/>";
      echo "</form>";
    }
    else if ($_GET['act2']=="list")
    {
      echo "<br/>";
      users_list($data);
      foreach($data as $a)
      {
        echo html_safe($a)." (<a href='admin.php?act=users&act2=remove&username=".html_safe($a)."'>remove</a>)<br/>\n";
      }
    }
  }
  else if ($_GET['act']=="files")
  {
    echo "<a href='admin.php?act=files&act2=list'>list files</a> | ";
    echo "<a href='admin.php?act=files&act2=new'>new file</a> | ";
    echo "<a href='admin.php?act=files&act2=edit'>edit file</a> | ";
    echo "<a href='admin.php?act=files&act2=remove'>remove file</a><br/>\n";
    
    if ($_GET['act2']=="new")
    {
      if ($_GET['name']!="")
      {
        files_new($_GET['name'],$_GET['status'],$_GET['assigned']);
        echo "<br/>";
        echo "new file '".html_safe($_GET['name'])."' with status '".html_safe($_GET['status'])."' added";
      }
      echo "<form action='admin.php' method='get'>";
      echo "<input type='hidden' name='act' value='files'/><br/>";
      echo "<input type='hidden' name='act2' value='new'/><br/>";
      echo "<label>new file name: </label>";
      echo "<input type='text' name='name' value=''/><br/>";
      echo "<label>new file status: </label>";
      echo "<select name='status'>";
        echo "<option value ='WAITING' selected='selected'>WAITING</option>";
        echo "<option value ='BUSY'>BUSY</option>";
        echo "<option value ='DONE'>DONE</option>";
      echo "</select><br/>";
      echo "<label>new file assigned to: </label>";
      echo "<input type='text' name='assigned' value=''/><br/>";
      echo "<input type='submit' value='add file'/><br/>";
      echo "</form>";
    }
    else if ($_GET['act2']=="edit")
    {
      if ($_GET['stage']==2)
      {
        files_edit($_GET['id'],$_GET['name'],$_GET['status'],$_GET['assigned']);
        echo "<br/>";
        echo "saved to node '".html_safe($_GET['name'])."' with status '".html_safe($_GET['status'])."'";
      }
      else if ($_GET['stage']==1)
      {
        if ($_GET['id']=="")
        {
          $id=files_getid($_GET['name']);
        }
        else
        {
          $id=$_GET['id'];
        }
        echo "<form action='admin.php' method='get'>";
        echo "<input type='hidden' name='act' value='files'/><br/>";
        echo "<input type='hidden' name='act2' value='edit'/><br/>";
        echo "<input type='hidden' name='id' value='".html_safe($id)."'/><br/>";
        echo "<input type='hidden' name='stage' value='2'/><br/>";
        echo "<label>file name: </label>";
        echo "<input type='text' name='name' value='".html_safe(files_getname($id))."'/><br/>";
        echo "<label>file type: </label>";
        echo "<select name='status'>";
          echo "<option value ='WAITING'".((files_getstatus($id)=="WAITING") ? " selected='selected'" : "").">WAITING</option>";
          echo "<option value ='BUSY'".((files_getstatus($id)=="BUSY") ? " selected='selected'" : "").">BUSY</option>";
          echo "<option value ='DONE'".((files_getstatus($id)=="DONE") ? " selected='selected'" : "").">DONE</option>";
        echo "</select><br/>";
        echo "<label>file assigned to: </label>";
        echo "<input type='text' name='assigned' value='".html_safe(files_getassigned($id))."'/><br/>";
        echo "<input type='submit' value='save file'/><br/>";
        echo "</form>";
      }
      else
      {
        echo "<form action='admin.php' method='get'>";
        echo "<input type='hidden' name='act' value='nodes'/><br/>";
        echo "<input type='hidden' name='act2' value='edit'/><br/>";
        echo "<input type='hidden' name='stage' value='1'/><br/>";
        echo "<label>node name: </label>";
        echo "<input type='text' name='name' value=''/><br/>";
        echo "<input type='submit' value='next'/><br/>";
        echo "</form>";
      }
    }
    else if ($_GET['act2']=="remove")
    {
      if ($_GET['id']!="" or $_GET['name']!="")
      {
        if ($_GET['id']=="")
        {
          $id=files_getid($_GET['name']);
        }
        else
        {
          $id=$_GET['id'];
        }
        
        $name=files_getname($id);
        files_remove($id);
        echo "<br/>";
        echo "removed file '".html_safe($name)."'";
      }
      echo "<form action='admin.php' method='get'>";
      echo "<input type='hidden' name='act' value='files'/><br/>";
      echo "<input type='hidden' name='act2' value='remove'/><br/>";
      echo "<label>file name: </label>";
      echo "<input type='text' name='name' value=''/><br/>";
      echo "<input type='submit' value='remove file'/><br/>";
      echo "</form>";
    }
    else if ($_GET['act2']=="list")
    {
      echo "<form action='admin.php' method='get'>";
      echo "<input type='hidden' name='act' value='files'/><br/>";
      echo "<input type='hidden' name='act2' value='list'/><br/>";
      echo "<input type='text' name='filter' value='".html_safe($_GET['filter'])."'/>";
      echo "<input type='submit' value='filter'/><br/>";
      echo "</form>";
      
      echo "<br/>";
      files_list($data,$_GET['filter']);
      foreach($data as $a)
      {
        echo "file: <a href='view.php?id=".$a."'>".html_safe(files_getname($a))."</a> status: ".html_safe(files_getstatus($a))." (<a href='admin.php?act=files&act2=edit&stage=1&id=".$a."'>edit</a>) (<a href='admin.php?act=files&act2=remove&id=".$a."'>remove</a>)<br/>";
      }
    }
  }
  else if ($_GET['act']=="log")
  {
    echo "<a href='admin.php?act=log&act2=clear'>clear log</a><br/>\n";
    
    if ($_GET['act2']=="clear")
    {
      log_clear();
      echo "<br/>";
      echo "log cleared";
    }
    
    echo "<br/>";
    log_print();
  }
  else if ($_GET['act']=="misc")
  {
    echo "<a href='admin.php?act=misc&act2=optimize'>optimize db</a><br/>\n";
    
    if ($_GET['act2']=="optimize")
    {
      db_optimize();
      echo "<br/>";
      echo "db optimized";
    }
  }
  else
  {
    echo "logged in";
  }
}
else
{
  echo "<form action='admin.php' method='post'>";
  echo "<table>";
  echo "<tr><td>username:</td>";
  echo "<td><input type='text' name='username' value=''/></td></tr>";
  echo "<tr><td>password:</td>";
  echo "<td><input type='password' name='password' id='password'/></td></tr>";
  echo "</table>";
  echo "<input type='submit' name='login' value='login'/>";
  echo "</form>";
}

require("footer.php");

