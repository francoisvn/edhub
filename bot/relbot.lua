-- release bot
-- version 1.4
-- by francois van niekerk

-- run the setup.sh script

local version = "1.4"
local boturl = "http://localhost/release/bot.php"

local base = _G

module("relbot")
base.require('luadchpp')
local adchpp = base.luadchpp

base.assert(base['access'], 'access.lua must be loaded and running before relbot.lua')
local access = base.access

local os = base.require('os')
local io = base.require('io')
local string = base.require('string')
local autil = base.require('autil')
local http = base.require("socket.http")

local cm = adchpp.getCM()
local pm = adchpp.getPM()
local sm = adchpp.getSM()

local announce_time = os.date("%Y-%m-%d_%H:%M:%S");
local announce_lastid = "0";

function string:split(sSeparator, nMax, bRegexp)
  if not (sSeparator ~= '') then
    return
  end
  if not (nMax == nil or nMax >= 1) then
    return
  end

  local aRecord = {}

  if self:len() > 0 then
    local bPlain = not bRegexp
    nMax = nMax or -1

    local nField=1 nStart=1
    local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
    while nFirst and nMax ~= 0 do
      aRecord[nField] = self:sub(nStart, nFirst-1)
      nField = nField+1
      nStart = nLast+1
      nFirst,nLast = self:find(sSeparator, nStart, bPlain)
      nMax = nMax-1
    end
    aRecord[nField] = self:sub(nStart)
  end

  return aRecord
end

function string:urlescape(s)
  s = string.gsub(s, "([&=+%c#])", function (c)
      return string.format("%%%02X", string.byte(c))
    end)
  s = string.gsub(s, " ", "+")
  return s
end


function broadcast(message)
  local entities = cm:getEntities()
  local size = entities:size()
  if size == 0 then
    return
  end

  local mass_msg = adchpp.AdcCommand(adchpp.AdcCommand_CMD_MSG, adchpp.AdcCommand_TYPE_INFO, adchpp.AdcCommand_HUB_SID)
  mass_msg:setFrom(adchpp.AdcCommand_HUB_SID)
  mass_msg:addParam(message)
  --mass_msg:addParam("PM", adchpp.AdcCommand_fromSID(mass_msg:getFrom()))

  for i = 0, size - 1 do
    local user = entities[i]:asClient()
    if user then
      mass_msg:setTo(user:getSID())
      user:send(mass_msg)
    end
  end
  --cm:sendToAll(mass_msg:getBuffer())
end

function do_announcements()
  local response = http.request(boturl.."?announce=1&time="..announce_time.."&lastid="..announce_lastid)
  local data = string.split(response,'|',3)
  if #data<3 then
    return
  end
  
  announce_time = data[1]
  announce_lastid = data[2]
  
  if data[3]~="-" then
    broadcast(data[3])
    do_announcements()
  end
end

access.commands.releases = {
  alias = { rel = true },

  command = function(c, parameters)
	  local response = http.request(boturl.."?rel=1")
	  autil.reply(c, response)
	  
	  do_announcements()
  end,

  help = "- display new releases",

  user_command = {
    name = "Relbot"..autil.ucmd_sep.."Releases",
    params = {}
  }
}

access.commands.relsearch = {
  command = function(c, parameters)
	  local response = http.request(boturl.."?search=1&q="..string:urlescape(parameters))
	  autil.reply(c, response)
	  
	  do_announcements()
  end,

  help = "query - perform a search",

  user_command = {
    name = "Relbot"..autil.ucmd_sep.."RelSearch",
    params = {autil.ucmd_line("query")}
  }
}

access.commands.refresh = {
  command = function(client, parameters)
	  do_announcements()
  end
}

rel_motd = cm:signalState():connect(function(entity)
  if entity:getState() == adchpp.Entity_STATE_NORMAL then
    local nick = entity:getField("NI")
    autil.reply(entity, "relbot "..version.." welcomes you, "..nick)
    autil.reply(entity, "say +rel or +relsearch to use me")
    do_announcements()
  end
end)

