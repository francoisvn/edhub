#! /bin/bash

if [[ `whoami` != "root" ]]
then
  echo "[relbot setup] this script requires root access, run with sudo"
  exit
fi

echo "[relbot setup] installing lua packages..."

apt-get install liblua5.1-socket2

echo "[relbot setup] creating symlinks..."

mkdir -p /usr/local/lib/lua/5.1/socket
mkdir -p /usr/local/lib/lua/5.1/mime

ln -s /usr/share/lua/5.1/socket.lua /usr/local/lib/lua/5.1/socket.lua
ln -s /usr/share/lua/5.1/mime.lua /usr/local/lib/lua/5.1/mime.lua
ln -s /usr/share/lua/5.1/ltn12.lua /usr/local/lib/lua/5.1/ltn12.lua

ln -s /usr/share/lua/5.1/socket/ftp.lua /usr/local/lib/lua/5.1/socket/ftp.lua
ln -s /usr/share/lua/5.1/socket/http.lua /usr/local/lib/lua/5.1/socket/http.lua
ln -s /usr/share/lua/5.1/socket/smtp.lua /usr/local/lib/lua/5.1/socket/smtp.lua
ln -s /usr/share/lua/5.1/socket/tp.lua /usr/local/lib/lua/5.1/socket/tp.lua
ln -s /usr/share/lua/5.1/socket/url.lua /usr/local/lib/lua/5.1/socket/url.lua

ln -s /usr/lib/lua/5.1/socket/core.so /usr/local/lib/lua/5.1/socket/core.so
ln -s /usr/lib/lua/5.1/socket/unix.so /usr/local/lib/lua/5.1/socket/unix.so
ln -s /usr/lib/lua/5.1/mime/core.so /usr/local/lib/lua/5.1/mime/core.so

echo "[relbot setup] done"

echo "[relbot setup] now do the following:"
echo "[relbot setup]   copy relbot.lua to adchpp/bin/Scripts"
echo "[relbot setup]   edit adchpp/config/Script.xml"
echo "[relbot setup]   restart the server or reload the scripts"


